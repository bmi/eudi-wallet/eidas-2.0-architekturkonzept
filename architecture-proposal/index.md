# Architecture Proposal for the German eIDAS Implementation

As part of the consultation process for the implementation of the EU Digital
Identity Wallet in Germany, this document provides a thorough exploration of
potential architecture options and describes various related considerations.

## Links

* The GitLab repository for this document is [hosted on
  OpenCoDE](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/).
* For reading, the content is available at [this
  website](https://bmi.usercontent.opencode.de/eudi-wallet/eidas-2.0-architekturkonzept/).

## Providing Feedback

At this step, we are interested in receiving feedback on the architecture and
the proposed options such as the following:

* Are the descriptions of the options clear and understandable?
* Are there any flaws in the proposed architecture options?
* Feedback regarding the security, privacy, user experience, potential user
   reach, and implementation complexity of the proposed options.
* Any other comments and suggestions.

To provide feedback, please file an [Issue on
OpenCoDE](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/-/issues).
You can find more information on how to contribute in the [CONTRIBUTING.md](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/-/blob/main/CONTRIBUTING.md?ref_type=heads).

## Changes

All changes to this document are tracked in the [CHANGELOG.md](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/-/blob/main/CHANGELOG.md?ref_type=heads).
