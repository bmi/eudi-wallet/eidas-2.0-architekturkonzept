<!-- PDF-show
\newpage
-->

# Introduction

eIDAS v2 aims at enabling Union citizens and residents of the EU to manage their
digital identity and a variety of other electronic attestations of attributes
(certificates, prescriptions, tickets, ...) in a European Digital Identity
Wallet (EUDIW).

The architecture of the EUDI ecosystem and the set of technical standards
envisioned to ensure interoperability among the different parties are specified
in the [Architecture and Reference
Framework](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework)
(ARF).

This document builds on the eIDAS regulation and the ARF (as of writing, Version
1.2). It proposes an architecture for the German national Wallet implementation
and presents design options for implementing the different functions provided by
an EUDIW. Furthermore, it clarifies concepts and describes details in the
context of the German implementation.

This document shall serve as a basis for discussions in the course of an ongoing
consultation process, especially regarding data privacy, security, and
operational model. Therefore, this document provides various architectural ideas
that are either suitable as a target solution for a decentralized wallet or can
serve as an idea for a solution on how to achieve such a target solution.

The architecture proposal takes into account the specifics of the German eID
system, the decentralized nature of registers in Germany, and the use cases and
requirements contributed through the consultation process.

The objective is to ensure the interoperability of the German national Wallet
implementation within the EUDI ecosystem across all member states, while
fulfilling the desired security, privacy, and user experience requirements.

The goal of this entire process and the architecture proposal is therefore to
ultimately have possible implementation options for a decentralized, secure and
privacy-friendly wallet. The document does not claim to actually provide the
corresponding implementation, as the corresponding future changes to the ARF or
the publication of the Implementing Acts could also have a significant impact on
an architecture that is unfortunately not yet foreseeable at this time.

## Overview

European Digital Identity Wallets (EUDIW) as envisioned by the ARF and this
document shall provide a set of well-defined, generic functions. Use cases are
implemented by ecosystem participants on top of these functions as required.
Figure 1 illustrates this concept.

![Figure showing a Wallet and its connections to an Issuer, a Relying Party and
additional services; Use cases 1, 2, and 3 affect different subsets of these
entities.](figures/figure1.svg)

<!-- PDF-hide-start -->
*Figure 1: The Wallet provides a set of generic functions to enable various use
cases, here shown in three examples.*
<!-- PDF-hide-end -->

### Wallet Functions

The following functions are defined:

- **PID Issuance and Presentation**: The EUDIW shall enable the user to identify
   and authenticate themselves to a Relying Party by receiving and presenting
   their Person Identification Data (PID). (See [Wallet Function: PID Issuance
   and Presentation](./functions/00-pid-issuance-and-presentation/index.md) for
   details.)

- **Qualified or non-qualified Electronic Attestations of Attributes ((Q)EAA)
   Issuance and Presentation**: The EUDIW shall enable the user to receive,
   manage and present (qualified) electronic attestations of attributes in a
   generic manner, i.e., any (Q)EAA compatible to the technical standards
   defined in this document can be stored in the EUDIW and subsequently
   presented to any compatible Relying Party.\
   (See [Wallet Function: (Q)EAA Issuance and
   Presentation](./functions/01-qeaa-issuance-and-presentation/index.md) for
   details.)

- **QES**: The EUDIW must support the identification of the signatory and their
   non-repudiable consent to initiate a QES. (See [QES](./05-services.md#qes)
   for details.)

- **Login**: The EUDIW shall enable the user to login into services by
   authenticating to a Relying Party. (See [Wallet Function:
   Login](./functions/02-pseudonyms/index.md) for details.)

### Use Case Examples

*Use Case 1:* In the example "Use Case 1", an ecosystem participant uses the
Wallet login function only, so it acts as a Relying Party towards the EUDIW.
There is no dependency on any other ecosystem participant. The Wallet will
provide the functionality to prove possession of certain cryptographic keys. The
rest of the user experience and logic must be provided in the use case
implementation.

*Use Case 2:* In example "Use Case 2", ecosystem participants use the functions
to issue and present EAAs. This means there need to be one or more Issuers for a
certain EAA type or multiple EAA types and there are one or more Relying Parties
processing presentations of those EAAs. The lifecycle management process for
those EAAs is out of scope of this document, the lifecycle requirements depend
on the specific domain. The Wallet will ensure a certain security (resistance)
level as requested by the Issuer of the EAA. The Wallet provides the core
functions to request and retrieve an EAA as well as to present an EAA in
response to an RP request. The Wallet will also display all relevant information
about the transaction (i.e., which data is requested by whom and for what
purpose) to the user and get the user's consent for the transaction. The
embedding of these functions into the overall user experience of the use case is
out of scope of the Wallet.

*A visual example of this use case can be viewed under the following link to
make the process described above more tangible. The design of the screens is to
be understood as an exemplary realization and not as a specification. The focus
of the screens provided is to present the use case of an exemplary bank account
opening step by step in a visually comprehensible way.*

![User Journey: Bank account opening](./user_journeys/UseCase_bank_account_opening.png)

*Use Case 3:* Besides the standard roles (Holder, Issuer, Relying Party) there
can be any number of services offered to ecosystem participants by leveraging
the Wallet functions. Remote signature creation, electronic archiving, and
payment APIs are some examples. The example "Use Case 3" illustrates this
concept. There, EAAs are used to authenticate the user at such an additional
service. A remote signature creation service could, for example, be built on top
of the "Online Identification with PID" Wallet function.

### Extending Protocols and Use Cases

The functions provided by the Wallet use a set of pre-defined standard
protocols. Wallets can, however, decide to implement other protocols as required
for use cases, especially if the use case requires a domain specific standard
(e.g., car keys).

The presented Wallet architecture does not (in general) implement use case
specific functionality. It is an electronic identity and generic infrastructure
component whose role to the ecosystem is to provide key and attestation
management capabilities on the required security levels. It shall be possible to
roll out new use cases (within the boundary of the Wallet functions and
supported technical protocols and formats) without the need to change the
Wallet's implementation.

## Used Standards

Below is the list of standards and specifications used in this document:

- [OAuth 2.0 Attestation-Based Client Authentication - draft
  3](https://www.ietf.org/archive/id/draft-ietf-oauth-attestation-based-client-auth-03.html)
- [OpenID4VC High Assurance Interoperability Profile with SD-JWT VC - draft
  0](https://openid.net/specs/openid4vc-high-assurance-interoperability-profile-sd-jwt-vc-1_0-00.html)
- [Selective Disclosure for JWTs (SD-JWT) - draft
  8](https://www.ietf.org/archive/id/draft-ietf-oauth-selective-disclosure-jwt-08.html)
- [SD-JWT-based Verifiable Credentials (SD-JWT VC) - draft
  3](https://www.ietf.org/archive/id/draft-ietf-oauth-sd-jwt-vc-03.html)
- [Designated Verifier Signatures for JOSE - draft
  1](https://www.ietf.org/archive/id/draft-bastian-dvs-jose-01.html)
- [OpenId for Verifiable Credential Issuance - draft
  14](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-14.html)
- [OpenID for Verifiable Presentations - draft
  21](https://openid.net/specs/openid-4-verifiable-presentations-1_0-21.html)
- [ISO/IEC 18013-5:2021](https://www.iso.org/standard/69084.html)

This document does not preclude implementing additional standards or specifications, but the
different flows and examples in this proposal are solely based on the above-mentioned
standards.

## Terminology

This document uses the following terms and abbreviations:

- **Two Factor Authentication (2FA):** *2FA is a security system that requires
  two distinct forms of identification in order to access something.*

- **European Digital Identity Wallet Architecture and Reference Framework
  (ARF):** *A toolbox including a technical Architecture and Reference Framework
  (ARF), a set of common standards and technical specifications, and a set of
  common guidelines and best practices.* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **Authentication:** *The process of verifying that the claimed identity is
  valid. It involves proving that the user is indeed who they claim to be.*
  [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)

- **Authorization:** *The process of granting or denying specific permissions or
  access rights to resources, systems, or data based on the authenticated
  identity of a user or entity. It determines what an authenticated user or
  entity is allowed to do.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)

- **brainpool:** *A set of elliptic curve domain parameters over finite prime
  fields that are recommended for use in cryptographic applications.*
  [RFC5639](https://datatracker.ietf.org/doc/html/rfc5639)

- **OAuth 2.0 Demonstrating Proof of Possession (DPoP):** *DPoP binds a token to
  the client that requested it to protect against misuse.*
  [DPoP](https://datatracker.ietf.org/doc/draft-ietf-oauth-dpop/)

- **Electronic Attestation of Attributes (EAA):** *‘Electronic Attestation of
  Attributes’ means an attestation in electronic form that allows the
  authentication of attributes.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)
  Article 3 (44)

- **Elliptic-curve Diffie–Hellman (ECDH):** *Elliptic-curve Diffie–Hellman
  (ECDH) is a key agreement protocol that allows two parties, each having an
  elliptic-curve public–private key pair, to establish a shared secret over an
  insecure channel. This shared secret may be directly used as a key, or to
  derive another key.* [RFC6090](https://datatracker.ietf.org/doc/html/rfc6090)

- **German eID system (eID):** *Architecture for the German electronic Identity
  Card and electronic Resident Permit is specified in the BSI Technical
  Guideline TR-03127.* [BSI
  TR-03127](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03127/TR-03127_node.html)

- **eID-Server (eID-Server):** *eID-Server for Online-Authentication based on
  Extended Access Control Version 2 (EAC2) between an eService and an eIDAS
  token, e.g. the German National Identity Card, the German electronic Residence
  Permit, the eID-Card for Union Citizens or an EAC-compatible mobile eID based
  on secure storage on a mobile device, that was derived from one of the former
  documents.* [BSI
  TR-03130](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03130/TR-03130_node.html)

- **eIDAS regulation version 2 (eIDAS):** *Regulation of the European Parliament
  and of the Council amending Regulation (EU) No 910/2014 as regards
  establishing a framework for a European Digital Identity.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)

- **European Digital Identity Wallet (EUDIW):** *‘European Digital Identity
  Wallet’ means an electronic identification, which allows the user to securely
  store, manage and validate identity data and electronic attestations of
  attributes, to provide them to relying parties and to other users of European
  Digital Identity Wallets, and to sign by means of qualified electronic
  signatures or to seal by means of qualified electronic seals.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)

- **(European Digital Identity) Wallet Provider (EUDIW Provider, Wallet
  Provider):** *The obligation to provide an EUDIW arises from the eIDAS
  Regulation. The MS determines whether it provides the EUDIW itself, mandates
  the provider(s) or recognizes one or more Wallet Provider(s) by another
  organization.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)

- **embedded UICC (eUICC):** *A removable or non-removable UICC which enables
  the secure remote and/or local management of Profiles.*
  [GSMA](https://www.gsma.com/get-involved/working-groups/sim-working-group/embedded-sim)

- **Hash-based message authentication code (HMAC):** *HMAC is a specific type of
  message authentication code involving a cryptographic hash function and a
  secret cryptographic key.*
  [RFC6234](https://datatracker.ietf.org/doc/html/rfc6234)

- **Identity Owner (Holder):** *The entity, such as a natural person, a legal
  person, or a device, which is subject of verifiable credentials from
  credential issuers and being in control of the reception, storage, and sharing
  of such credentials with relying parties.* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **Hardware Security Module (HSM):** *A HSM is a device for providing
  cryptographic functionalities whereas the life cycle of cryptographic keys and
  the performance of cryptographic functions is managed within a highly
  protected hardware environment.*

- **Identification:** *The process of claiming or recognizing an identity. It
  involves presenting an identifier that represents an individual or entity.*
  [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)

- **Internet Engineering Task Force (IETF):** *The Internet Engineering Task
  Force is a standards organization for the Internet and is responsible for the
  technical standards that make up the Internet protocol suite. It has no formal
  membership roster or requirements and all its participants are volunteers.*

- **International Organization for Standardization (ISO):** *The International
  Organization for Standardization is an independent, non-governmental,
  international standard development organization composed of representatives
  from the national standards organizations of member countries.*

- **PID presentation format (ISO mdoc):**

- **Issuer:** *An entity that issues credentials to a Holder. Sometimes referred
  to as Provider.*
  [OpenID4VCI](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html)

- **JSON Web Token (JWT):** *JSON Web Token (JWT) is a compact, URL-safe means
  of representing claims to be transferred between two parties. The claims in a
  JWT are encoded as a JSON object that is used as the payload of a JSON Web
  Signature (JWS) structure or as the plaintext of a JSON Web Encryption (JWE)
  structure, enabling the claims to be digitally signed or integrity protected
  with a Message Authentication Code (MAC) and/or encrypted.*
  [RFC7519](https://datatracker.ietf.org/doc/html/rfc7519)

- **Level of Assurance (LoA):** [eIDAS Levels of Assurance
  (LoA)](https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/eIDAS+Levels+of+Assurance),
  [BSI
  TR-03107](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Technische-Richtlinien/TR-nach-Thema-sortiert/tr03107/TR-03107_node.html)

- **Message Authentication Code (MAC):** *The result of a HMAC performance.*
  [RFC8446](https://datatracker.ietf.org/doc/html/rfc8446)

- **OpenID for Verifiable Credential Issuance (OpenID4VCI):** *OAuth protected
  API for the issuance of Verifiable Credentials.*
  [OpenID4VCI](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html)

- **OpenID for Verifiable Presentations (OpenID4VP):** *A mechanism on top of
  OAuth 2.0 [RFC6749](https://datatracker.ietf.org/doc/html/rfc6749) that
  enables presentation of Verifiable Credentials as Verifiable Presentations.*
  [OpenID4VP](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html)

- **Person Identification Data (PID):** *‘Person Identification Data’ means a
  set of data, issued in accordance with Union or national law, enabling the
  identity of a natural or legal person, or of a natural person representing a
  natural or legal person, to be established.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)

- **Person Identification Data Issuer (PID Provider (PI)):** *A Member State or
  other legal entity issuing Person Identification Data to Users for later use.*
  [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **Person Identification Data Provider (PID Provider (PP)):** *A Member State
  or other legal entity providing Person Identification Data to Users.* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **Public Key Infrastructure (PKI):** *Systems, software, and communication
  protocols that are used by EUDI Wallet ecosystem components to distribute,
  manage, and control public keys. A PKI publishes public keys and establishes
  trust within an environment by validating and verifying the public keys
  mapping to an entity.* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **Proof of Possession (PoP):** *Evidence provided by the Wallet regarding the
  possession of the respective key material.*

- **Qualified Electronic Attestation of Attributes (QEAA):** *‘Qualified
  Electronic Attestation of Attributes’ means an electronic attestation of
  attributes, which is issued by a qualified trust service provider and meets
  the requirements laid down in Annex V.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)
  Article 3 (45)

- **Qualified Electronic Signature (QES):** *‘Qualified Electronic Signature’
  means an advanced electronic signature that is created by a qualified
  electronic signature creation device, and which is based on a qualified
  certificate for electronic signatures.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)

- **Qualified Trust Service Provider (QTSP):** *A Trust Service Provider who
  provides one or more Qualified Trust Services and is granted the qualified
  status by the supervisory body.* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **Relying Party (RP):** *‘Relying Party’ means a natural or legal person that
  relies upon an electronic identification, European Digital Identity Wallets or
  other electronic identification means, or a trust service.* [eIDAS
  2](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0117_EN.html)
  Article 3 (6)

- **Selective Disclosure for JWT (SD-JWT):** *A composite structure, consisting
  of an Issuer-signed JWT (JWS,
  [RFC7515](https://datatracker.ietf.org/doc/html/rfc7515)), Disclosures and
  optionally a Key Binding JWT that supports selective disclosure.* [SD-JWT
  (Draft)](https://datatracker.ietf.org/doc/draft-ietf-oauth-selective-disclosure-jwt/)

- **SD-JWT-based Verifiable Credentials (SD-JWT VC):** *Verifiable Credentials
  with JSON payloads with and without selective disclosure based on the SD-JWT
  format.* [SD-JWT
  (Draft)](https://datatracker.ietf.org/doc/draft-ietf-oauth-selective-disclosure-jwt/)

- **Secure Element (SE):** *Secure Elements are physical components in
  electronic devices that securely store and protect sensitive data and
  applications and may provide certain secure cryptographic operations.* [Secure
  Elements for mobile
  platforms](https://www.bsi.bund.de/EN/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/Secure-Elements/secure-elements_node.html)

- **Seed Credential:** *A credential derived from the electronic Identity Card.
  It is used to request a fresh PID credential created on-demand from the PID
  Provider.*

- **Smart-eID (Smart-eID):** *The smart eID enables citizens to store their
  electronic proof of identity directly in their smartphones.* [Smart-eID
  Act](http://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&jumpTo=bgbl121s2281.pdf)

- **Trusted List (Trusted List):** *Repository of information about
  authoritative entities in a particular legal or contractual context which
  provides information about their current and historical status.* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **Trust Service Provider (TSP):** *A natural or a legal person who provides
  one or more Trust Services, either as a qualified or as a non-qualified Trust
  Service Provider.* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **Interacting entity (User):** *A natural or legal person using an EUDI
  Wallet. Also referred to as Holder.* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)

- **User Experience / User Interface (UX/UI):** *The user interaction work flow
  (UX) and the user interaction functional and visual elements (UI) of a
  software and/or hardware application.* [Wikipedia User experience
  design](https://en.wikipedia.org/wiki/User_experience_design)

- **Verifiable Credential (VC):** *A credential created by an Issuer in a way
  that the integrity and authenticity of the credential can be cryptographically
  verified.*
  [OpenID4VCI](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html)

- **Wallet Secure Cryptographic Device (WSCD):** *Hardware-backed secure
  environment for creating, storing, and/or managing cryptographic keys and
  data. Examples include Secure Elements (SE), Trusted Execution Environments
  (TEEs), and (remote or local) Hardware Security Module (HSM).* [ARF
  1.2.0](https://github.com/eu-digital-identity-wallet/eudi-doc-architecture-and-reference-framework/tree/v1.2.0)
