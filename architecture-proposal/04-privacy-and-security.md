<!-- PDF-show
\newpage
-->

# Privacy and Security

To ensure a secure and privacy-preserving EUDI Wallet design, the core
properties listed in the following are desirable. They are either intended by
the eIDAS regulation or are based on general principles and good practices of
privacy and security.

Architecture designs for the various Wallet functions (see Sections [Wallet
Function: PID Issuance and
Presentation](./functions/00-pid-issuance-and-presentation/index.md), [Wallet
Function: (Q)EAA Issuance and
Presentation](./functions/01-qeaa-issuance-and-presentation/index.md), and
[Wallet Function: Login](./functions/02-pseudonyms/index.md)) will vary in the
degree to which they can fulfill these properties. Furthermore, not all
properties are equally applicable for all functions and use cases. This shall be
assessed during the consultation process and be taken into account when
evaluating the different design options.

## Privacy Requirements

For system design and comparison of different solutions options we consider the
following core privacy properties:

### Unobservability

Unobservability refers to the property where an adversary cannot discern any
useful information about a communication or transaction. This ensures that
sensitive data, such as message content, sender or receiver identity, or any
other relevant information, remains hidden from unauthorized parties. In this
context, neither Wallet Providers nor Issuers shall be able to track where a
user uses their credentials or learn details concerning the attributes provided.

For Wallet providers, this implies that the Wallet must run on a device under
the user's control; in practice, this usually means a native app on a
smartphone. Backend (server) components of the Wallet provider, if used, must
not be able to track where users present their credential or learn details
concerning the attributes of transactions.

For Issuers, this implies that there must be no involvement of the Issuer in the
presentation process (e.g., for ad-hoc issuance or revocation checks) unless it
can be ensured that the Issuer cannot track where the user presents their
credential or learn details concerning the attributes of the presentation.

### Unlinkability

Unlinkability refers to the property that it cannot be distinguished whether two
transactions are related to the same user or not.

Different definitions of unlinkability can be considered in this context (not
all are equally applicable to all functions and use cases):

* **Outsider**: An adversary that may observe or modify the transactions should
not be able to link two transactions to the same user.
* **Relying Party**: A Relying Party should not be able to link two presentation
transactions to the same user (unless sufficiently identifying information is
part of the presented credential).
* **Issuer**: An Issuer should not be able to link two issuance transactions to
the same user (unless the user provides sufficiently identifying information as
part of its authentication).
* **Colluding Relying Parties**: Two Relying Parties should not be able to link
two presentation transactions to the same user by sharing the received
presentations.
* **Colluding Issuers**: Two Issuers should not be able to link two issuance
transactions to the same user by sharing the received information during
issuance (e.g. wallet attestations).
* **Colluding Relying Party and Issuer**: An Issuer and a Relying Party should
not be able to link an issuance and presentations session to the same user
(unless the user provides sufficiently identifying information as part of their
authentication to the issuer and as part of the presented credential).

Issuance and presentation protocols should support unlinkability and ensure that
cryptographic keys and random numbers cannot be used as correlation identifiers,
this also includes less obvious data fields such as timestamps or version
numbers.

If a Relying Party requires to recognize a user across multiple transactions, it
should use pseudonyms.

### Data Minimization and Prevention of Overidentification

According to the principle of data minimization, Relying Parties shall only
receive the data they need for the specific transaction. Various properties can
help to achieve this:

* **Pseudonymity:** Pseudonymity refers to the possibility of using a pseudonym
   when authenticating online or presenting credentials, unless the
   identification is required by law.
* **Selective Disclosure:** Selective Disclosure is the property to disclose
 only selected attributes of a credential during the presentation. This means
   that the Wallet (with the user's consent) can present a selected subset of
   the data fields (claims) in a credential while other fields are not revealed
   to the Relying Party.
* **Enforced Disclosure Limitation:** Enforced Disclosure Limitation means that
   the scope of data Relying Parties can request can be limited by having
   Relying Parties register the data fields they need for a specific type of
   transaction and that the limitation can be enforced by the Wallet.

The following mechanisms are not considered in this proposal:

* **Zero-Knowledge Proofs:** Zero-knowledge proofs can be used to prove that a
   certain claim is true without revealing the actual value of the claim. For
   example, a user could prove that they are over 18 without revealing their
   exact date of birth. Zero-knowledge proofs are not supported by the
   credential formats chosen in the ARF and bring significant implementation and
   security challenges, especially as they are not in widespread use yet.
* **Optional Data in User Consent:** Some user consent implementations allow
   users to select which data fields they want to disclose to a Relying Party,
   out of those requested by the RP. While this can be seen as a privacy
   feature, for users, however, it can be difficult to understand which data
   fields are really required by the RP and what the consequences are of not
   disclosing certain data fields. When users encounter errors and have to
   repeat steps in the transaction process, it can be expected that they would
   refrain from unselecting any data fields in the long run. Therefore, such an
   option could lead to overidentification in the long run as Relying Parties
   can exploit user fatigue. Consequently, it seems preferable to ensure (by
   technical means and data privacy laws) that Relying Parties only request the
   data they absolutely need and not support optional data in user consent
   screens.

### Repudiation

Repudiation (or “plausible deniability”, since “repudiation” actually refers to
a single act of dispute although commonly used as synonym for the general
ability to deny transactions) refers to the property that one of the entities
involved in an identification transaction can plausibly deny to a third party
(i.e., a party not involved in the transaction) in having participated in the
transaction after its completion, or can plausibly deny to a third party having
provided certain data. The ability to deny the transaction towards third parties
does not impact the reliability of the transaction towards the Relying Party
involved. Below, two repudiation variants are considered.

* **Deniability of Data Authenticity:** Deniability of Data Authenticity refers
to the property that the authenticity of a credential and its attributes
provided by the Issuer can be plausibly denied to a third party after a
presentation. In this context, it is required that a Relying Party should not be
able to prove to a third party the authenticity of a credential and the
integrity of its attributes that it has previously verified.
* **User deniability:** User deniability refers to the role of the user in a
presentation transaction and the user's authentication of the presentation. In
this context it is required that a Relying Party should not be able to prove to
a third party that the user has presented a credential in context of an
identification, where the Relying Party was previously involved as verifier.

**Note on repudiation in relation to identification:**  General understanding of
identification processes is their short-lived nature. Presenting one's ID to a
verifier in the analogue world has the immediate outcome for both not being able
to prove the affair to someone else, as long as the ID has not been copied.
Repudiation would achieve the same feature for digital identities.

The importance of repudiation for digital identifications might not be obvious,
its absence however reveals a string of implications. In case of data breaches
with authenticated PID involved, plausible deniability to the public would
become highly favorable to the persons affected, especially if the involved
Relying Party could arouse social discomfort. Regarding long-term storage, data
leaks are generally more a matter of time rather than probability, since this
risk cannot be thoroughly eliminated. Data thieves would obviously have a
preference for guaranteed genuine copies over unauthenticated PID.

In some use cases, where records need to be kept by law, a long-living,
non-repudiable declaration of intent is required to be verified by some third
party; however, the specific requirements encountered by these use cases must be
scrutinized. Usually, these declarations differ from identifications, so other
means like QES are likely to be more appropriate. In any case, if a Relying
Party has the requirement to prove an identification to a third party, it is the
responsibility of the Relying Party to document and certify the identification
procedure to the third party.

From an academic point of view, there is a lack of studies that examine the
value of repudiation in connection with identification transactions compared to
the impact that already arises from the leak of the collected data itself (e.g.,
use-case specific data in the context of record-keeping requirements). The value
of non-repudiation may be limited since the authenticity of leaked data can
often be asserted by other means, e.g., by verifying samples of the data against
publicly available data. In this context it should also be considered what
influence the fulfillment of other requirements, such as unlinkability, could
have on the impact of non-repudiation and to what extent the strict application
of the GDPR could minimize the risk of amassing authenticated data at Relying
Parties.

The repudiation requirement and the appropriateness of using authenticated PID
is subject of ongoing discussions within the consultation process.

## Security Requirements

For system design and comparison of different solutions options we consider the
following core security properties:

### Level of Assurance (LoA)

Level of Assurance refers to the degree of confidence in the processes thus
providing assurance that the user that uses a particular identity is in fact the
user to which that identity was assigned.  This property refers to the [LoA of
the eIDAS
Regulation](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A02015R1502-20220711).

### Unforgeability of Credentials

Unforgeability of credentials refers to the property that a credential can only
be created by the legitimate Issuer and cannot be tampered by someone else. This
property ensures the integrity and authenticity of a credential and its
verification.

### Freshness of Presentations

Freshness of presentations refers to the property that every presentation must
be created anew for every verification. This property refers to the mechanisms
used for binding a presentation to the presentation request of the Relying Party
to prevent replay of presentations.

### User Binding of presentations

User Binding refers to the property that credentials are under the sole control
of the user. This property refers to the mechanism used for secure
authentication of the user in the context of using the credential (e.g., by
two-factor authentication linked with a cryptographic binding to the credential)
to prevent unauthorized issuance or presentation of a credential. In the
literature, this property is also described as holder binding.

### Session Integrity

Session Integrity refers to the property that an attacker is not able to insert
themselves into an authentication exchange between a Verifier and a Wallet or
into an issuance process between a Wallet and an Issuer. This property requires
mechanisms for binding communication to an authenticated session to prevent
session hijacking.

<!-- PDF-show
\newpage
-->

## Cryptographic algorithms

This section defines the cryptographic algorithms that should be used and are
based on the recommendation of the
[BSI](https://www.bsi.bund.de/SharedDocs/Downloads/EN/BSI/Publications/TechGuidelines/TG02102/BSI-TR-02102-1.pdf?__blob=publicationFile&v=7)
and
[SOG-IS](https://www.sogis.eu/documents/cc/crypto/SOGIS-Agreed-Cryptographic-Mechanisms-1.3.pdf).

* Issuer Signatures, DeviceSigned:
    * [ECDSA](https://tools.ietf.org/html/rfc6090) using
    [brainpoolP512r1](https://datatracker.ietf.org/doc/html/rfc5639) and
    [SHA-512](https://datatracker.ietf.org/doc/html/rfc6234)
    * [ECDSA](https://tools.ietf.org/html/rfc6090) using
    [brainpoolP384r1](https://datatracker.ietf.org/doc/html/rfc5639) and
    [SHA-384](https://datatracker.ietf.org/doc/html/rfc6234)
    * [ECDSA](https://tools.ietf.org/html/rfc6090) using
    [brainpoolP256r1](https://datatracker.ietf.org/doc/html/rfc5639) and
    [SHA-256](https://datatracker.ietf.org/doc/html/rfc6234)
    * [ECDSA](https://tools.ietf.org/html/rfc6090) using
    [secp256r1](http://www.secg.org/sec2-v2.pdf) and
    [SHA-256](https://datatracker.ietf.org/doc/html/rfc6234)
* Key binding/presentation and Verifier Signatures:
    * [ECDSA](https://tools.ietf.org/html/rfc6090) using
    [brainpoolP384r1](https://datatracker.ietf.org/doc/html/rfc5639) and
    [SHA-384](https://datatracker.ietf.org/doc/html/rfc6234)
    * [ECDSA](https://tools.ietf.org/html/rfc6090) using
    [secp256r1](http://www.secg.org/sec2-v2.pdf) and
    [SHA-256](https://datatracker.ietf.org/doc/html/rfc6234)
* MAC:
    * [ECDH](https://tools.ietf.org/html/rfc7748) with
    [secp256r1](http://www.secg.org/sec2-v2.pdf) +
    [hmac-sha2](https://datatracker.ietf.org/doc/html/rfc6234)
* Encryption:
    * [AES-256-GCM](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38d.pdf)
* Hashing, PKCE (code challenge method):
    * [SHA-256](https://datatracker.ietf.org/doc/html/rfc6234)
* DPoP:
    * [ECDSA](https://tools.ietf.org/html/rfc6090) using
    [secp256r1](http://www.secg.org/sec2-v2.pdf) and
    [SHA-256](https://datatracker.ietf.org/doc/html/rfc6234)

Note: The Brainpool curves currently do not have identifiers in the IANA
registry for JOSE. Since there are multiple algorithms for the issuer to sign a
credential, the relying party and the wallet provider needs to implement all of
them. The same applies for the key binding/presentation.

In a later version of this document, we will evaluate if we can reduce the
number of algorithms to reduce complexity.
