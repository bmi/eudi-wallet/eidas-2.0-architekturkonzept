<!-- PDF-show
\newpage
-->

# (Q)EAA Issuance with OpenID4VC

## Basic Idea

The Q(EAA) Provider issues signed credentials to the Wallet which are stored for
a longer period. The Wallet can then present the credentials (including a proof
of possession by signing over a wallet-held key) to the Relying Party.

The flows include examples for the credential formats SD-JWT VC and mdoc, but
other formats are not precluded.

## Cryptographic Formats

Long-Term keys:

- (Q)EAA Provider has long-term key pair $(ep\_pub, ep\_priv)$ which is used to
  sign the Credential

Transaction-specific keys:

- EUDIW generates device key $(device\_pub, device\_priv)$ which is used to
  generate proof of possession of a Credential
- EUDIW optionally generates key pair $(cre\_eph\_pub, cre\_eph\_priv)$ which is
  used to encrypt the credential response

Artifacts:

- EUDIW fetches Wallet Attestation from Wallet Provider: $wallet\_attestation$
- (Q)EAA Provider issues
    - SD-JWT VC (plus Disclosures): $sd\_jwt :=
      \text{sign}(\mathit{hashed\_attestation\_data}, device\_pub)_{ep\_priv}$
    - ISO mdoc: $mdoc := \text{sign}(\mathit{hashed\_attestation\_data},
      device\_pub)_{ep\_priv}$

### Dependencies

*TODO: May want to expand to include metadata.*

```plantuml
digraph G {

  subgraph cluster_eaa_provider {
    style=filled
    color=lightblue
    label="(Q)EAA Provider"
    ep [label="🗝 (ep_priv, ep_pub)"]
    user_data [label="attestation data"]
  }

  subgraph cluster_wallet {
    style=filled
    color=lightgreen
    label="Wallet"
    nonce_audience [label="nonce, audience"]
    device_key [label="🗝 (device_priv, device_pub)"]
  }
  ep -> device_key [label="sign (SD-JWT VC/\nmdoc issuerAuth)"]
  ep -> user_data [label="sign (SD-JWT VC/\nmdoc issuerAuth)"]

  device_key -> nonce_audience [label="sign (KB-JWT/\nmdoc deviceAuth)"]
}
```

Note: Wallet attestation not shown in this chart.

<!-- PDF-show
\newpage
-->

## Remote Issuance with Authorization Code Flow

### Sequence Diagram

[User Journey: (Q)EAA Issuance - Authorization
Code](../user_journeys/QEAA-AuthorizationCode-issuance.png)

```plantuml

@startuml
'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300


'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title (Q)EAA Issuance over OpenID4VCI with Authorization Code Flow

actor u as "User\nOpenID Holder"
participant w as "User's EUDI Wallet Instance"
participant b as "Browser App\n(same device)"
participant i  [
                (Q)EAA Provider
                ----
                ""Long-term Key: (//ep_pub//, //ep_priv//)""
            ]


alt Wallet initiated
  u --> w : open wallet, unlock wallet
  hnote over w #dfd: Screen: launch_wallet
  hnote over w #dfd: Screen: unlock_wallet
  u --> w : Select issuance of pre-configured (Q)EAA
  note right: Pre-configured (Q)EAAs contains credential_issuer URLs and scopes
  hnote over w #dfd: Screen: credential_catalog
  hnote over w #dfd: Screen: next_steps
else Issuer initiated
  u -> b : Open Issuer's Website
  b -> i : Query website: HTTP GET <credential_issuer_url>/
  i -> b : HTTP 200\nHTML with static Credential Offer\n<Grant-Type: Authorization Code>
  hnote over w #dfd: Screen: same_device_issuer_website
  u --> w : open wallet, unlock wallet
  hnote over w #dfd: Screen: launch_wallet
  hnote over w #dfd: Screen: unlock_wallet
  hnote over w #dfd: Screen: next_steps
end

w -> i : tls() HTTP GET ./well-known Credential Issuer Metadata
i -> w : tls() HTTP 200 <Credential Issuer Metadata>

alt #ffd optional Wallet Attestation
  w -> i : tls() HTTP POST </session_endpoint> wallet attestation nonce
  i -> i : generate and store nonce
  i -> w : tls() HTTP 200 <wallet attestation nonce>
  w -> w : get wallet attestation from Wallet Provider backend (wallet attestation nonce)
  w -> w : generate PoP for wallet attestation
end

'newpage

w -> i : tls() PAR (code_challenge, wallet attestation + PoP, redirect_uri, either scope or authorization_details)
alt #ffd optional Wallet Attestation
  i -> i : verify wallet attestation\ncheck wallet solution status on trust list
end
i -> w : tls() request_uri

w -> b : tls() HTTP GET <OpenID4VCI Authorization Request(request_uri)>
b -> i : tls() HTTP GET <OpenID4VCI Authorization Request(request_uri)>
group User Authentication
  b <-> i : <user authentication>
  note over i,b : the (Q)EAA Provider may perform any form of user authentication in the browser, this may be:\n- username/password\n- OpenID4VP credential presentation\n- taking a foto\n- any other mechanism enabled through the browser
  hnote over b #dfd: Screen: same_device_issuer_authentication
end
i -> b : tls() HTTP 302 redirect-uri <OpenID4VCI Authorization Response(authorization code)>
b -> w: tls() HTTP 302 redirect-uri <OpenID4VCI Authorization Response(authorization code)>

u --> w : user consent for receiving the (Q)EAA
hnote over w #dfd: Screen: consent_add_credential

w -> i : tls() HTTP POST <Token Request(authorization code, PKCE code_verifier, wallet attestation + PoP, DPoP Header)>
i -> i : lookup authorization code\ngenerate TokenResponse with DPoP access token\nverify PKCE challenge
alt #ffd optional Wallet Attestation
  i -> i : verify wallet attestation
end
i -> w : tls() HTTP 200 <Token Response(DPoP-bound accesss_token, c_nonce, optional authorization_details)>

w -> w : generate cryptographic binding key pair (//device_pub//, //device_priv//) and proof of possession with c_nonce
note right: Device key should be protected by platform security mechanisms
hnote over w #dfd: Screen: device_authenticator

'newpage

alt #ffd optional credential response encryption
  w -> w : generate credential response encryption key pair (//cre_eph_pub//, //cre_eph_priv//)
  w -> w : create credential_response_encryption object with jwk containing //cre_eph_pub//
end

w -> i : tls() HTTP POST <Credential Request(DPoP-bound access token, //device_pub//, optionally credential_response_encryption)>
i -> i : lookup access token\n validate key proof
alt #dfd SD-JWT VC (Q)EAA
  i -> i : generate SD-JWT VC (Q)EAA and Disclosures with attestation data and //device_pub// as cnf key, sign with //ep_priv//
else #ddf mdoc (Q)EAA
  i -> i : generate mdoc (Q)EAA with attestation data and //device_pub// as deviceKey, sign with //ep_priv//
end

alt credential_response_encryption present
  i -> i : generate encrypted credential response JWT using the values received in the credential_response_encryption object
  i -> w : tls() HTTP 200 <JWT(Credential Response((Q)EAA))>
  w -> w : decrypt credential response JWT and retrieve credential
else not present
  i -> w : tls() HTTP 200 <Credential Response((Q)EAA)>
end

w -> w : store (Q)EAA
hnote over w #dfd: Screen: success
hnote over w #dfd: Screen: home

note over w : it might be useful to have a redirect in the browser

@enduml
```

<!-- PDF-show
\newpage
-->

### Step-by-Step Description

1. **(Wallet initiated)** The user opens and unlocks his wallet.
2. **(Wallet initiated)** The user navigates to a credential catalog of
   pre-configured (Q)EAA Providers and offered credentials in the Wallet. He
   selects a (Q)EAA and the Wallet continues the flow with the preconfigured
   Credential Issuer URL and credential identifier.
3. **(Issuer initiated)** User browses to (Q)EAA Provider's website.
4. **(Issuer initiated)** Browser app on the user's device opens the (Q)EAA
   Provider's website
5. **(Issuer initiated)** (Q)EAA Provider returns an HTML page to the browser
   containing a Credential Offer; containing
    - the Credential Issuer URL
    - an identifier to the offered credential
    - an optional issuer_state parameter to bind this issuance to a pre-existing
      session
6. **(Issuer initiated)** The user clicks on the link, the Wallet launches and
   he unlocks the wallet
7. The Wallet fetches the Credential Issuer's metadata from the ./well-known
   files of the Credential Issuer URL
8. The (Q)EAA Provider (from here on named Issuer in his OpenID4VCI role) return
   the Credential Issuer Metadata; containing:
    - technical information about the Issuer
    - translations and display data for the offered credentials
9. The Wallet shows information about the (Q)EAA Provider and the offered (Q)EAA
   to the user and asks for consent
10. The Wallet requests a fresh nonce for the wallet attestation nonce (wallet
     attestation)
11. The Issuer generates a fresh nonce linked to the issuance session
12. The Issuer returns the wallet attestation nonce to the Wallet
13. The Wallet fetches fresh wallet attestation from the Wallet Provider backend
14. The Wallet generates proof of possession (PoP) using the nonce fetched in
    the previous step
15. The Wallet sends a Pushed Authorization Request (PAR) to the Issuer;
    containing:
    - the Wallet Provider's client_id
    - a PKCE code_challenge
    - the wallet attestation and proof of possession
    - a redirect_uri containing an app-link
    - either scope or an authorization_details parameter requesting the (Q)EAA
16. The Issuer verifies the wallet attestation and validates the status of the
    Wallet solution through a trust list
17. The Issuer stores the Authorization Request, generates a request_uri and
    sends this back to the Wallet
18. The Wallet uses the request_uri to create an Authorization Request and send
    this to the Authorization endpoint of the Issuer by issuing a HTTP GET
    request over the system's default browser.
19. The Browser sends the Authorization Request to the Issuer
20. In this authentication phase, the Issuer may exchange any information over
    the browser to the user, as he got the "screen control". This may be, but is
    not limited to:
    - username / password
    - OpenID4VP credential presentation of PID, other (Q)EAA, etc..
    - taking a photo
21. The Issuer finalizes the authentication phase by responding with an
    Authorization Response; containing
    - the auth code
    - the redirect provided by the wallet in the PAR
22. The browser follows the redirect, launching again the wallet
23. The Wallet sends a Token Request to the (Q)EAA Provider; containing:
    - the auth code from Authorization Response
    - the PKCE code_verifier matching the code_challenge from Authorization
      Request
    - the wallet attestation and Proof of possession
    - a DPoP key
24. The Issuer matches the code, verifies the PKCE code_verifier to the
    previously received code_challenge and verifies the wallet attestation. It
    then generates an access token bound to the DPoP key.
25. The Issuer verifies the wallet attestation and proof of possession
26. The Issuer sends a Token Response; containing
    - DPoP-bound access token
    - a c_nonce
    - an authorization_details object, in case the authorization_details
      parameter was used in the Authorization Request
27. The Wallet generates a key pair for the cryptographic binding of the (Q)EAA
    (*kb_eph_pub*, *kb_eph_priv*) and signs the c_nonce
28. The Wallet may request an encrypted credential response. In order to do so,
    it generates a new ephemeral keypair (*cre_eph_pub*, *cre_eph_priv*).
29. The Wallet creates the `credential_response_encryption` JSON object
    containing the following information:
    - a jwk containing the *cre_eph_pub*
    - the JWE alg parameter
    - the JWE enc parameter
30. The Wallet sends the Credential Request to the Issuer; containing
    - the DPoP-bound access token
    - the proof of cryptographic binding key *kb_eph_pub* signed c_nonce
    - and optionally the `credential_response_encryption` object
31. The Issuer validates the access_token and the proof
32. **(SD-JWT VC (Q)EAA)** The Issuer creates the Disclosures from attestation data
    and signs the SD-JWT VC with *ep_priv* containing
    - attestation data and hashed Disclosures as the user claims
    - *kb_eph_pub* as cnf claim He then appends the Disclosures.
33. **(mdoc (Q)EAA)** The Issuer creates the hashed Releases from attestation
    data and signs the mdoc with *ep_priv* containing
    - attestation data and hashed Releases as issuerAuth
    - *kb_eph_pub* as deviceKeyInfo
34. In case `credential_response_encryption` information is present, the Issuer
    creates an encrypted JWT (JWE) using the values received in the
    `credential_response_encryption` object and adds it (among others) the
    (Q)EAA credential to the payload.
35. The Issuer sends the Credential Response JWT; containing:
    - The (Q)EAA as credential
36. The Wallet decrypts the Credential Response JWT using the *cre_eph_priv* and
    retrieves the credential.
37. In case `credential_response_encryption` information are not present, the
    Issuer sends the plain Credential Response; containing:
    - The (Q)EAA as credential.
38. The Wallet stores the (Q)EAA and the associated keys.

<!-- PDF-show
\newpage
-->

## Remote Issuance Pre-Authorized Code Flow

### Sequence Diagram

[User Journey: (Q)EAA Issuance - Pre-Authorized
Code](../user_journeys/QEAA-PreauthorizedCode-issuance.png)

```plantuml

@startuml
'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title (Q)EAA Issuance over OpenID4VCI with Pre-Authorized Code Flow

actor u as "User\nOpenID Holder"
participant w as "User's EUDI Wallet Instance"
participant b as "Browser App"
participant i  [
                (Q)EAA Provider
                ----
                ""Long-term Key: (//ep_pub//, //ep_priv//)""
            ]


u -> b : Open Issuer's Website
group User Authentication
  note over i,b : the (Q)EAA Provider may perform any form of user authentication prior to creating the credential offer
end
b -> i : Query website: HTTP GET <credential_issuer_url>/
i -> b : HTTP 200\nHTML with dynamic Credential Offer\n<pre-authorized code, tx_code>

alt Issuer initiated - Same Device
  hnote over w #dfd: Screen: same_device_issuer_website
  u --> w : open wallet, unlock wallet
  hnote over w #dfd: Screen: launch_wallet
  hnote over w #dfd: Screen: unlock_wallet
else Issuer initiated - Cross Device
  hnote over w #dfd: Screen: cross_device_issuer_website
  u --> w : scan QR-Code
  hnote over w #dfd: Screen: launch_wallet
  hnote over w #dfd: Screen: unlock_wallet
  hnote over w #dfd: Screen: device_camera
end

w -> i : tls() HTTP GET ./well-known Credential Issuer Metadata
i -> w : tls() HTTP 200 <Credential Issuer Metadata>
u --> w : user consent for receiving the (Q)EAA
hnote over w #dfd: Screen: consent_add_credential

'newpage

alt #ffd optional Wallet Attestation
  w -> i : tls() HTTP POST </session_endpoint> wallet attestation nonce
  i -> i : generate and store nonce
  i -> w : tls() HTTP 200 <wallet attestation nonce>
  note over w : nonce may be provided in credential offer or reuse pre-auth code
  w -> w : perform and get wallet attestation from Wallet Provider backend (wallet attestation nonce)
  w -> w : generate PoP for wallet attestation
end

w -> i : tls() HTTP POST <Token Request(pre-authorization code, wallet attestation, DPoP Header, optional txCode)>
alt #ffd optional Wallet Attestation
  i -> i : verify wallet attestation\ncheck wallet solution status on trust list
end
i -> i : lookup authorization code\ngenerate TokenResponse with DPoP access token
i -> w : tls() HTTP 200 <Token Response(DPoP-bound accesss_token, c_nonce)>

w -> w : generate cryptographic binding key pair (//device_pub//, //device_priv//) and proof of possession with c_nonce
note right: Device key should be protected by platform security mechanisms
hnote over w #dfd: Screen: device_authenticator

alt #ffd optional credential response encryption
  w -> w : generate credential response encryption key pair (//cre_eph_pub//, //cre_eph_priv//)
  w -> w : create credential_response_encryption object with jwk containing //cre_eph_pub//
end

'newpage

w -> i : tls() HTTP POST <Credential Request(DPoP-bound access token, //device_pub//, optionally credential_response_encryption)>
i -> i : lookup access token\n validate key proof
alt #dfd SD-JWT VC (Q)EAA
  i -> i : generate SD-JWT VC (Q)EAA and Disclosures with attestation data and //device_pub// as cnf key, sign with //ep_priv//
else #ddf mdoc (Q)EAA
  i -> i : generate mdoc (Q)EAA with attestation data and //device_pub// as deviceKey, sign with //ep_priv//
end

alt credential_response_encryption present
  i -> i : generate encrypted credential response JWT using the values received in the credential_response_encryption object
  i -> w : tls() HTTP 200 <JWT(Credential Response((Q)EAA))>
  w -> w : decrypt credential response JWT and retrieve credential
else not present
  i -> w : tls() HTTP 200 <Credential Response((Q)EAA)>
end

w -> w : store (Q)EAA

hnote over w #dfd: Screen: success
hnote over w #dfd: Screen: home

note over w : it might be useful to have a redirect in the browser for same-device flow

@enduml
```

<!-- PDF-show
\newpage
-->

### Step-by-Step Description

1. User browses to (Q)EAA Provider's website. At this point it is assumed that
   the user authentication already took place before the OpenID4VCI protocols
   starts. This may happen through any mechanism, e.g. username/password,
   providing OTPs, etc..
2. Browser app on the user's device opens the (Q)EAA Provider's website
3. (Q)EAA Provider returns a HTML page to the browser containing a Credential
   Offer; containing
    - the Credential Issuer URL
    - an identifier to the offered credential
    - a pre-Authorized Code for authorization, linked to the identified user
    - the flag `tx_code` indicating whether the Wallet is expected to provide a
      Transaction Code in the Token Request. The Transaction Code is sent by the
      Issuer over a second channel, e.g., via e-mail or SMS, and helps to
      strengthen security by additional session binding.
4. **(Same-Device)** The user clicks on the link, the Wallet launches and he
   unlocks the wallet
5. **(Cross-Device)** The user opens and unlocks his wallet and scans the
   QR-Code
6. The Wallet fetches the Credential Issuer's metadata from the ./well-known
   files of the Credential Issuer URL
7. The (Q)EAA Provider (from here on named Issuer in his OpenID4VCI role) return
   the Credential Issuer Metadata; containing:
    - technical information about the Issuer
    - translations and display data for the offered credentials
8. The Wallet shows information about the (Q)EAA Provider and the offered (Q)EAA
   to the user and asks for consent
9. The Wallet requests a fresh nonce for the wallet attestation nonce (wallet
    attestation)
10. The Issuer generates a fresh nonce linked to the issuance session
11. The Issuer returns the wallet attestation nonce to the Wallet
12. The Wallet fetches fresh wallet attestation from the Wallet Provider backend
13. The Wallet generates proof of possession (PoP) using the nonce fetched in
    the previous step
14. The Wallet sends a Token Request to the (Q)EAA Provider; containing:
    - the pre-authorized code from Authorization Response
    - the wallet attestation and Proof of possession
    - a DPoP key
    - the Transaction Code that the user received over a second communication
      channel from the Issuer, if `tx_code` was present in the Credential Offer
15. The Issuer verifies the wallet attestation and validates the status of the
    Wallet solution through a trust list
16. The Issuer matches the pre-authorized code and generates an access token
    bound to the DPoP key.
17. The Issuer sends a Token Response; containing
    - DPoP-bound access token
    - a c_nonce
18. The Wallet generates a key pair for the cryptographic binding of the (Q)EAA
    (*kb_eph_pub*, *kb_eph_priv*) and signs the c_nonce
19. The Wallet may request an encrypted credential response. In order to do so,
    it generates a new ephemeral keypair (*cre_eph_pub*, *cre_eph_priv*).
20. The Wallet creates the `credential_response_encryption` JSON object
    containing the following information:
    - a jwk containing the *cre_eph_pub*
    - the JWE alg parameter
    - the JWE enc parameter
21. The Wallet sends the Credential Request to the Issuer; containing
    - the DPoP-bound access token
    - the proof of cryptographic binding key *kb_eph_pub* signed c_nonce
    - and optionally the `credential_response_encryption` object
22. The Issuer validates the access_token and the proof
23. **(SD-JWT VC (Q)EAA)** The Issuer creates the Disclosures from attestation data
    and signs the SD-JWT VC with *ep_priv* containing
    - attestation data and hashed Disclosures as the user claims
    - *kb_eph_pub* as cnf claim He then appends the Disclosures.
24. **(mdoc (Q)EAA)** The Issuer creates the hashed Releases from attestation
    data and signs the mdoc with *ep_priv* containing
    - attestation data and hashed Releases as issuerAuth
    - *kb_eph_pub* as deviceKeyInfo
25. In case `credential_response_encryption` information is present, the Issuer
    creates an encrypted JWT (JWE) with the information and adds (among others)
    the (Q)EAA credential to the payload.
26. The Issuer sends the Credential Response JWT; containing:
    - The (Q)EAA as credential
27. The Wallet decrypts the Credential Response JWT using the *cre_eph_priv* and
    retrieves the credential.
28. In case `credential_response_encryption` information are not present, the
    Issuer sends the plain Credential Response; containing:
    - The (Q)EAA as credential.
29. The Wallet stores the (Q)EAA and the associated keys.

## Usability Considerations

### Common Considerations

- (Q)EAA Provider / Credential catalogue should inform users in advance of what
  is required for the successful issuance of the (Q)EAA and what steps follow
- For reasons of transparency and to increase trust, (Q)EAA Providers should
  provide sufficient information (metadata) for the consent screen. This allows
  users to learn everything relevant e.g. about the provider itself.
- User must confirm the process with the Device Authenticator
- User can have a (Q)EAA credential on several end devices at the same time

### Authorization Code Flow Considerations

- User needs to authenticate successfully on (Q)EAA Provider website
- If the user has to go through more than 2 context switches (issuer initiated
  flow), it can be confusing, might create unnecessary complexity and additional
  potential points of failure, and ultimately leads to friction.

### Pre-Authorized Code Flow Considerations

- User is already authenticated on (Q)EAA Provider website or no authentication
  is required
- To obtain/copy the transaction code, an additional context switch may be
  necessary

## Privacy Considerations

- Unlinkability has to be achieved with batch issuance

## Security Considerations

- (Q)EAA Providers may choose to require a wallet attestation
- the security guarantees of the Pre-Authorized Code Flow, especially in the
  Cross-Device Flow, are not intended for higher security levels

## Implementation Consideration

*TBD*

## Requirements for QEAA Issuance

The issuance of (Q)EAA credentials has to fulfill the following requirements:

- the identification based on Art 24. eIDAS and ETSI TS 119 461
- the authentication acc. ETSI TS 119 471 and ETSI TS 119 472-1
- respect the requirements of [Annex V of the eIDAS Regulation](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=OJ:L_202401183#anx_%C2%A0V)
- the defined formats in the Implementing Acts (need reference)
- using the signature formats for the credential formats defined in ETSI
