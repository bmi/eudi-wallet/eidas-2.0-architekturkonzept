<!-- PDF-show
\newpage
-->

# Wallet Attestation

Wallet Attestations (also called Wallet Instance Attestation or Wallet Trust
Evidence) provide the PID or (Q)EAA Provider with the necessary information that
a Wallet Instance is authentic and trustworthy and fulfills the security
requirements.

## Lifecycle

A Wallet Attestation is associated by the Wallet Provider with a particular
Wallet Instance and is also associated with a particular Issuer/Relying Party by
the Wallet Instance.

The lifecycle of a Wallet Attestation is still being evaluated in the eIDAS
process. Two general concepts may be used:

- Ephemeral Wallet Attestations: A Wallet Attestation is used for a single
   transaction. A Wallet Attestation is required by the Issuer and the Wallet
   Instance ad-hoc requests a new one from the Wallet Provider Backend. For
   every issuance process a new wallet attestation is generated and no linking
   between these attestations is foreseen. Revocation of Wallet Attestations is
   not necessary for ephemeral Wallet Attestations.
- Long-lived Wallet Attestations: A Wallet Attestation is used for multiple
   transactions. A Wallet Attestation is required by the Issuer and the Wallet
   Instance either requests a new one from the Wallet Provider Backend or reuses
   an existing one. Revocation of Wallet Attestations is foreseen and shall be
   regularly checked by PID and (Q)EAA Providers.

The details of the Wallet Attestation and the corresponding PID/(Q)EAA
lifecycles are TBD.

## Protocols

Using Wallet Attestations within the [OpenID for Verifiable Credentials protocol
family](https://openid.net/sg/openid4vc/) are recommended to use the mechanism
described in [OAuth 2.0 Attestation-Based Client
Authentication](https://datatracker.ietf.org/doc/draft-ietf-oauth-attestation-based-client-auth/).
This enables the Wallet to send a Wallet Attestation (called wallet attestation
in the OpenID scope) within the Pushed Authorization Request (PAR) or within the
Token Request. The specification is yet to be finalized, so the details are
subject to change.

## Attestation format and content

The recommended format for a Wallet Attestation is currently described in the
[OpenID4VC High Assurance Interoperability Profile with SD-JWT
VC](https://openid.net/specs/openid4vc-high-assurance-interoperability-profile-sd-jwt-vc-1_0.html).
The specification is yet to be finalized as well, so the details are subject to
change. The contents of a Wallet Attestation should include:

- the issuer, i.e. the Wallet Provider Backend
- the subject, i.e. the client_id of the Wallet Solution
- validity information, i.e. the issuance and expiration dates
- the wallet instance' public key
- the attack potential resistance
- optionally specifying the used key type and user authentication mechanism
- optionally a list of device keys that may be used for key binding credentials

The Wallet Attestation should NOT include:

- information on technical capabilities of the wallet
- unique identifiers that enable linkability, e.g. information to the Wallet
  Provider's internal user account

Information within the Wallet Attestation shall be reduced to a minimum for
privacy and scalability reasons. Further information about the Wallet Solution
should be linked on the trust list:

- certification scheme
- certification status
- information on the Wallet Provider and the Wallet Solution

## Revocation

The Wallet Provider may revoke a long-lived Wallet Attestation in the following
cases:

- The Wallet Security Cryptographic Device (WSCD) has a known vulnerability
- The Wallet Instance application has a known vulnerability

To enable revocation for wallet attestations, the mechanisms described in the
[revocation section](../functions/05-status-management/index.md)
apply.

## Cryptographic Formats

Long-Term keys:

- Wallet Provider has long-term key pair $(wp\_pub, wp\_priv)$
- Wallet Security Cryptographic Device generates device key $(device\_pub,
  device\_priv)$

Artifacts:

- Wallet Provider Backend issues Wallet Attestation: $wallet\_attestation :=
  \text{sign}(\mathit{wallet\_data} | \mathit{device\_pub})_\text{wp\_priv}$

<!-- PDF-show
\newpage
-->

## Sequence Diagram

```plantuml

@startuml

'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title Issuance of Issuer specific Wallet Attestations

participant u [
                User
            ]
participant wscd [
                User's Wallet Security Cryptographic Device
                ----
                Long-term Key: (//device_pub//, //device_priv//)
            ]
participant w [
                User's Wallet Instance
                ----
                Wallet Attestion bound to //device_priv//
            ]
participant b  [
                User's Wallet Provider Backend
                ----
                Long-term Key: (//wp_pub//, //wp_priv//)
            ]

w -> b: tls() HTTP POST <getWalletAttestationNonces(optional wallet instance identifier)>
b -> b : generate and store nonces
b -> w: tls() HTTP 200 walletAttestationNonces(nonces)>

w -> wscd : generate new key (nonce)
u <--> wscd : check user authentication
note right: user authentication towards the WSCD may be cached for a short duration for further access to WSCD in the issuance process
hnote over w #dfd: Screen: Wallet User Authentication

wscd -> wscd : generate key pair: (//device_pub//, //device_priv//), including optional key attestations
wscd -> wscd : generate proof of possession: sign(nonce)<sub>//device_priv//</sub>

wscd -> w : //device_pub//, optional key attestation, proof of possession

w -> w : request app attestations(nonce) from OS attestation service
note right: this step involves connection to OS backend services

w -> b : tls() HTTP POST <getWalletAttestation(//device_pub//, PoP, key attestation, app attestation)>

b -> b : verify PoP, key and app attestation
b -> b : check device black list (Vulnerability Management)
b -> b : generate wallet attestation containing //device_pub// and sign with //wp_priv//
note left: wallet attestation as defined in "OpenID4VC High Assurance Interoperability Profile with SD-JWT VC - draft 00"

b -> w: tls() HTTP 200 <walletAttestation>

w -> w : store walletAttestation for Issuer's client_id

@enduml
```

<!-- PDF-show
\newpage
-->

## Step-by-Step Description

1. The Wallet Instance requests to perform a wallet attestation. It may provide
   a wallet instance identifier to the Wallet Provider Backend service, that may
   be used to match a (pseudonymous) user account.
2. The Wallet Provider Backend generates and stores a set of nonces linked to
   this wallet attestation process.
3. The Wallet Provider Backend returns the wallet attestation nonces to the
   Wallet Instance.
4. The Wallet Instance requests the generation of a new key pair from its Wallet
   Security Cryptographic Device (WSCD).
5. The WSCD checks the user authentication. If the User is not authenticated
   (from previous transactions) yet, the WSCD may initiate and perform a user
   authentication, e.g. with a PIN.
6. The WSCD generates an attestation key pair (*device_priv*, *device_pub*),
   which is stored in the secure, hardware-bound WSCD. The WSCD may generate a
   key attestation (if possible) using a nonce, that attests that this key pair
   is actually originating from a specific, certified type of WSCD.
7. The WSCD generates a proof of possession by signing a nonce (from Step 3)
   using *device_priv*.
8. The WSCD sends a response to the Wallet Instance, containing:
   - the attestation key *device_pub*
   - the proof of possession
   - the optional key attestation
9. The Wallet Instance performs Operating System specific attestation (also
   called app attestation) using a nonce to validate the status and integrity of
   the Wallet Instance. This step may involve contacting a backend attestation
   service of the Operating System.
10. The Wallet Instance requests a wallet attestation from the Wallet Provider
    Backend by sending:
    - the attestation key *device_pub*
    - the proof of possession
    - the optional key attestation
    - the app attestation
11. The Wallet Provider Backend validates the transmitted keys and attestations
12. The Wallet Provider Backend checks if the device on which the Wallet
    Instance is installed is listed on a device black list and is known as a
    vulnerable device / WSCD.
13. The Wallet Provider Backend generates wallet attestation using the
    attestation key *device_pub* and signing with *wp_priv*.
14. The Wallet Provider Backend returns the wallet attestation to the Wallet
    Instance.
15. The Wallet Instance stores the wallet attestation on the device and links
    the usage of the wallet attestation with a specific Issuer.
