<!-- PDF-show
\newpage
-->

# (Q)EAA Presentation with OpenID4VC

## Basic Idea

The Q(EAA) Provider issues signed credentials to the Wallet, which are stored
for a longer period. The Wallet can then present the credentials (including a
proof of possession by signing over a wallet-held key) to the Relying Party.

The flows are include examples for the credential formats SD-JWT VC and mdoc, but
are not limited to these.

## Cryptographic Formats

Long-Term keys:

* (Q)EAA Provider has long-term key pair $(ep\_pub, ep\_priv)$ that is used to
  sign SD-JWT VC
* RP has long-term key pair $(rp\_pub, rp\_priv)$ that is used to sign OpenID4VP
  Authorization Request
* Wallet app has device key pair $(device\_pub, device\_priv)$ that is attested
  by the EAA Provider are used to sign KB-JWT

Transaction-specific keys:

* RP has ephemeral key pair $(rp\_eph\_pub, rp\_eph\_priv)$ that is used to
  encrypt the response

Artifacts:

* $kb\_jwt := \text{sign}(nonce, audience, \text{hash}(sd\_jwt,
  disclosures))_{device\_priv}$
* $device\_Auth$ for mdoc

### Dependencies

*TODO: May want to expand to include metadata.*

```plantuml
digraph G {

  subgraph cluster_eaa_provider {
    style=filled
    color=lightblue
    label="(Q)EAA Provider"
    ep [label="🗝 (ep_priv, ep_pub)"]
    user_data [label="attestation data"]
  }

  subgraph cluster_wallet {
    style=filled
    color=lightgreen
    label="Wallet"
    nonce_audience [label="nonce, audience"]
    device_key [label="🗝 (device_priv, device_pub)"]
  }
  ep -> device_key [label="sign (SD-JWT VC/\nmdoc issuerAuth)"]
  ep -> user_data [label="sign (SD-JWT VC/\nmdoc issuerAuth)"]

  device_key -> nonce_audience [label="sign (KB-JWT/\nmdoc deviceAuth)"]
}
```

Note: Wallet attestation not shown in this chart.

<!-- PDF-show
\newpage
-->

## Same Device Flow

### Sequence Diagram

[User Journey: (Q)EAA Presentation - Same
Device](../user_journeys/QEAA-SameDevice-presentation.png)

```plantuml

@startuml
'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title (Q)EAA presentation over OpenID4VP (same device)

actor u as "User\nOpenID Holder"
participant b as "Browser App\n(same device)"
participant v [
                Relying Party
                ----
                ""Long-term Key: (//rp_pub//, //rp_priv//)""
]
participant w [
                User's EUDI Wallet Instance
                ----
                ""Device binding Key: (//device_pub//, //device_priv//)""
]

u --> b : browse to application
hnote over b #dfd: Screen: same_device_relying_party_start
b -> v : tls() HTTP GET <rp-website>

v -> v : generate new browser session with <color:#909>session_id</color>

v -> v : generate ephemeral key pair (//rp_eph_pub//, //rp_eph_priv//)
note right: //rp_eph_pub// is only used for response encryption

v -> v : create OpenID4VP Authorization Request,\n sign with //rp_priv//,\n store under <request_uri>,\nbind everything to <color:#909>session_id</color>
note left: Authorization Request includes:\n- presentation_definition\n- purpose\n- state\n- nonce

v -> b : tls() HTTP 200 HTML containing wallet-link openid4vp://authorize?")\nclient_id=..&request_uri=<request_uri>\nSet-Cookie: sid=<color:#909>session_id</color>
u --> b : action to start flow/launch wallet
b -> w : launch with wallet-link openid4vp://
hnote over w #dfd: Screen: launch_wallet

u --> w : unlock wallet
hnote over w #dfd: Screen: unlock_wallet

'newpage

w -> v : tls() HTTP GET <request_uri>
v -> w : tls() HTTP 200 <JWT-Secured Authorization Request(presentation_definition, nonce, state)>
w -> w : validate Authorization Request JWT using //rp_pub//
u --> w : user consent for presenting the (Q)EAA
hnote over w #dfd: Screen: consent_present_credential
alt #dfd SD-JWT VC (Q)EAA
  w -> w : generate KB-JWT by signing over nonce, audience with //device_priv//
  hnote over w #dfd: Screen: device_authenticator
  w -> w : generate SD-JWT VC presentation according to <presentation_definition> by removing unnecessary Disclosures and attaching KB-JWT
else #ddf mdoc (Q)EAA
  w -> w : generate deviceAuth by signing over SessionTranscript with //device_priv//
  hnote over w #dfd: Screen: device_authenticator
  w -> w : generate issuerAuth according to <presentation_definition> by removing unnecessary Releases and attaching deviceAuth
end

w -> w : create vp_token and presentation_submission

w -> v : tls() HTTP POST <Authorization Response(presentation_submission, vp_token, state)> (optionally encrypted to //rp_eph_pub//)
v -> v : optionally decrypt Authorization Response with //rp_eph_pub//
v -> v : look up state in existing sessions\ncreate & store response_code for session

v -> w : tls() HTTP 200 <redirect_uri incl. response_code>

w -> b : launch browser with <redirect_uri with response_code>
hnote over w #dfd: Screen: success_redirect

b -> v : tls() HTTP GET <redirect_uri with response_code>\nCookie: sid=<color:#909>session_id</color>

'newpage

v -> v : look up session with <color:#909>session_id</color> and match response_code
alt #dfd SD-JWT VC (Q)EAA
  v -> v : verify contents of <vp_token>:\n- SD-JWT VC signature from (Q)EAA Provider\n- KB-JWT with //kb_eph_pub// from SD-JWT VC\n- nonce and audience from KB-JWT
else #ddf mdoc (Q)EAA
  v -> v : verify contents of <vp_token>:\n- mdoc signature from (Q)EAA Provider\n- deviceAuth with //kb_eph_pub// from SD-JWT VC\n- nonce and audience from SessionTranscript
end


v -> b : tls() HTTP 200 <HTML with continued UX flow>
hnote over b #dfd: Screen: same_device_relying_party_identified

@enduml
```

<!-- PDF-show
\newpage
-->

### Step-by-Step Description

Note: While certain assumptions about session management of the Relaying Party
are made here, the concrete implementation is considered out of scope for this
document. The usual security considerations for web session management apply.

1. User browses to Relying Party (RP) website
2. Browser app on the user's device opens the RP website
3. The RP generates a new browser session, depicted `session_id`
4. RP generates a key pair to be used for ECDH key agreement for response
   encryption
5. RP generates an OpenID4VP Authorization Request, binding it internally to the
   `session_id` and stores it under a `request_uri` (e.g.,
   `https://rp.example.com/oidc/request/1234`);
   * The request is bound to the user's browser session
   * It is signed using a key bound to the RP's metadata that can be retrieved
     using the RP's client_id
   * It contains the presentation_definition containing the information about
     the requested (Q)EAAs
   * It contains RP's nonce and state
   * It contains the purpose given by the RP
6. RP returns a HTML page to the browser containing a link to the wallet app
   (e.g.,
   `openid4vp://authorize?client_id=..&request_uri=https://rp.example.com/oidc/request/1234`).
   It also sets a Cookie with the `session_id`.
7. The user clicks on the link
8. The wallet app is launched with the custom URI scheme
9. The user unlocks the wallet app (see notes below)
10. The wallet app retrieves the Authorization Request from the RP website
     (e.g., `https://rp.example.com/oidc/request/1234`)
11. The wallet app receives the Authorization Request
12. The wallet app validates the Authorization Request using the RP's public key
    * Was the signature valid and the key bound to the RP's metadata?
    * **Security:** This ensures that the Authorization Request was not tampered
      with; it does not ensure that the party that sent the Authorization
      Request is the RP.
13. The Wallet displays information about the identity of the Relying Party and
    the purpose, the user gives consent to present the (Q)EAA.
14. **(SD-JWT VC (Q)EAA)** The Wallet generates a KB-JWT by signing over nonce and
    audience with *device_priv*
15. **(SD-JWT VC (Q)EAA)** The Wallet creates the presentation according to
    presentation_definition by cutting out the unnecessary Disclosures and
    appending the KB-JWT
16. **(mdoc (Q)EAA)** The Wallet generates an mdoc deviceAuth by signing over
    SessionTranscript with *device_priv*
17. **(mdoc (Q)EAA)** The Wallet creates the presentation according to
    presentation_definition by cutting out the unnecessary Releases assembling
    the issuerAuth with deviceAuth
18. The wallet app creates a VP token and a presentation submission
19. The wallet app sends the VP token and presentation submission to the RP
    (encrypted to the RP's public key *rp_eph_pub*).
20. The RP optionally decrypts the Authorization Response with *rp_eph_pub*
    received in the Authorization Request.
21. The RP finds a session with the state, generates a response_code and stores
    the received vp_token
22. The RP responds with a redirect inluding the generated response_code.
23. The wallet launches the browser with the redirect_uri and response_code.
24. The browser receives the redirect_uri and response_code and sends an HTTP
    GET request to the RP frontend, this request contains the Cookie set in the
    beginning of the protocol.
25. The RP matches the session_id from the Cookie and matches the response_code
    to find the received vp_token.
26. **(SD-JWT VC (Q)EAA)** The RP verifies the content of the vp_token. In case of
    SD-JWT VC it validates the Issuer signature from the (Q)EAA Provider, verifies
    the KB-JWT matching to the cnf claim and validates that the KB-JWT is fresh
    and matches to its own client_id and nonce.
27. **(mdoc (Q)EAA)** The RP verifies the content of the vp_token. In case of
    mdoc it validates the Issuer signature from the (Q)EAA Provider, verifies
    the deviceAuth matching to the deviceKey and validates that the
    SessionTranscript is fresh and matches to its own client_id and nonce.
28. The RP considers the user to be identified in the session context and
    continues the UX flow.

<!-- PDF-show
\newpage
-->

## SCross Device Flow

### equence Diagram

[User Journey: (Q)EAA Presentation - Cross
Device](../user_journeys/QEAA-CrossDevice-presentation.png)

```plantuml

@startuml
'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title (Q)EAA presentation over OpenID4VP (cross device)

actor u as "User\nOpenID Holder"
participant b as "Browser App\n(other device)"
participant v [
                Relying Party
                ----
                ""Long-term Key: (//rp_pub//, //rp_priv//)""
]
participant w [
                User's EUDI Wallet Instance
                ----
                ""Device binding Key: (//device_pub//, //device_priv//)""
]

u --> b : browse to application
hnote over b #dfd: Screen: cross_device_relying_party_start
b -> v : tls() HTTP GET <rp-website>

v -> v : generate new browser session with <color:#909>session_id</color>

v -> v : generate ephemeral key pair (//rp_eph_pub//, //rp_eph_priv//)
note right: //rp_eph_pub// is only used for response encryption

v -> v : create OpenID4VP Authorization Request,\n sign with //rp_priv//,\n store under <request_uri>,\nbind everything to <color:#909>session_id</color>
note left: Authorization Request includes:\n- presentation_definition\n- purpose\n- state\n- nonce

v -> b : tls() HTTP 200 HTML containing wallet-link openid4vp://authorize?")\nclient_id=..&request_uri=<request_uri>\nSet-Cookie: sid=<color:#909>session_id</color>
u --> w : launch wallet
hnote over w #dfd: Screen: launch_wallet
u --> w : unlock wallet
hnote over w #dfd: Screen: unlock_wallet
b -> w : scan QR-Code with "openid4vp://..."
hnote over w #dfd: Screen: device_camera

w -> v : tls() HTTP GET <request_uri>
v -> w : tls() HTTP 200 <JWT-Secured Authorization Request(presentation_definition, nonce, state)>
w -> w : validate Authorization Request JWT using //rp_pub//
u --> w : user consent for presenting the (Q)EAA
hnote over w #dfd: Screen: consent_present_credential

'newpage

alt #dfd SD-JWT VC (Q)EAA
  w -> w : generate KB-JWT by signing over nonce, audience, and hash of SD-JWT VC and selected disclosures with //device_priv//
  hnote over w #dfd: Screen: device_authenticator
  w -> w : generate SD-JWT VC presentation according to <presentation_definition> by removing unnecessary Disclosures and attaching KB-JWT
else #ddf mdoc (Q)EAA
  w -> w : generate deviceAuth by signing over SessionTranscript with //device_priv//
  hnote over w #dfd: Screen: device_authenticator
  w -> w : generate issuerAuth according to <presentation_definition> by removing unnecessary Releases and attaching deviceAuth
end

w -> w : create vp_token and presentation_submission

w -> v : tls() HTTP POST <Authorization Response(presentation_submission, vp_token, state)> (optionally encrypted to //rp_eph_pub//)
v -> v : optionally decrypt Authorization Response with //rp_eph_pub//
v -> v : look up state in existing sessions and match session

'newpage

alt #dfd SD-JWT VC (Q)EAA
  v -> v : verify contents of <vp_token>:\n- SD-JWT VC signature from (Q)EAA Provider\n- KB-JWT with //kb_eph_pub// from SD-JWT VC\n- nonce and audience from KB-JWT
else #ddf mdoc (Q)EAA
  v -> v : verify contents of <vp_token>:\n- mdoc signature from (Q)EAA Provider\n- deviceAuth with //kb_eph_pub// from SD-JWT VC\n- nonce and audience from SessionTranscript
end

v -> w : tls() HTTP 200
note right: in cross device flow the redirect is omitted here
hnote over w #dfd: Screen: success

note over b,v : The Browser may refresh the RP website with polling or the RP website may send a signal, e.g. with previuosly established websocket

b -> v : tls() HTTP GET <rp-website>\nCookie: sid=<color:#909>session_id</color>
v -> v : look up session with <color:#909>session_id</color> and match verification result
v -> b : tls() HTTP 200 <HTML with continued UX flow>
hnote over b #dfd: Screen: cross_device_relying_party_identified

@enduml
```

<!-- PDF-show
\newpage
-->

### Step-by-Step Description

Note: While certain assumptions about session management of the Relaying Party
are made here, the concrete implementation is considered out of scope for this
document. The usual security considerations for web session management apply.

1. User browses to Relying Party (RP) website
2. Browser app on the user's device opens the RP website
3. The RP generates a new browser session, depicted `session_id`
4. RP generates a key pair to be used for ECDH key agreement for response
   encryption
5. RP generates an OpenID4VP Authorization Request, binding it internally to the
   `session_id` and stores it under a `request_uri` (e.g.,
   `https://rp.example.com/oidc/request/1234`);
   * The request is bound to the user's browser session
   * It is signed using a key bound to the RP's metadata that can be retrieved
     using the RP's client_id
   * It contains the presentation_definition containing the information about
     the requested (Q)EAAs
   * It contains RP's nonce and state
   * It contains the purpose given by the RP
6. RP returns a HTML page to the browser containing a QR-Code to the wallet app
   (e.g.,
   `openid4vp://authorize?client_id=..&request_uri=https://rp.example.com/oidc/request/1234`).
   It also sets a Cookie with the `session_id`.
7. The user launches his wallet and hits a button to scan QR-Code from the RP's
   website.
8. The wallet decodes the Authorization Request from the QR-Code and starts the
   OpenID4VP Flow.
9. The user unlocks the wallet app (see notes below)
10. The wallet app retrieves the Authorization Request from the RP website
     (e.g., `https://rp.example.com/oidc/request/1234`)
11. The wallet app receives the Authorization Request
12. The wallet app validates the Authorization Request using the RP's public key
    * Was the signature valid and the key bound to the RP's metadata?
    * **Security:** This ensures that the Authorization Request was not tampered
      with; it does not ensure that the party that sent the Authorization
      Request is the RP.
13. The Wallet displays information about the identity of the Relying Party and
    the purpose, the user gives consent to present the (Q)EAA.
14. **(SD-JWT VC (Q)EAA)** The Wallet generates a KB-JWT by signing over nonce,
    audience, and hash of SD-JWT VC and selected disclosures with *device_priv*
15. **(SD-JWT VC (Q)EAA)** The Wallet creates the presentation according to
    presentation_definition by cutting out the unnecessary Disclosures and
    appending the KB-JWT
16. **(mdoc (Q)EAA)** The Wallet generates an mdoc deviceAuth by signing over
    SessionTranscript with *device_priv*
17. **(mdoc (Q)EAA)** The Wallet creates the presentation according to
    presentation_definition by cutting out the unnecessary Releases assembling
    the issuerAuth with deviceAuth
18. The wallet app creates a VP token and a presentation submission
19. The wallet app sends the VP token and presentation submission to the RP
    (encrypted to the RP's public key *rp_eph_pub*).
20. The RP optionally decrypts the Authorization Response with *rp_eph_pub*
    received in the Authorization Request.
21. The RP matches the browser session with the state parameter.
22. **(SD-JWT VC (Q)EAA)** The RP verifies the content of the vp_token. In case of
    SD-JWT VC it validates the Issuer signature from the (Q)EAA Provider, verifies
    the KB-JWT matching to the cnf claim and validates that the KB-JWT is fresh
    and matches to its own client_id and nonce.
23. **(mdoc (Q)EAA)** The RP verifies the content of the vp_token. In case of
    mdoc it validates the Issuer signature from the (Q)EAA Provider, verifies
    the deviceAuth matching to the deviceKey and validates that the
    SessionTranscript is fresh and matches to its own client_id and nonce.
24. The RP responds with HTTP 200 and acknowledges the received vp_token.
25. The browser refreshes the RP's website, either by polling regularly or by
    receiving a signal from the RP, e.g. through websockets. It sends a HTTP GET
    request using the Cookie set in the beginning of the protocol.
26. The RP matches the session_id from the Cookie and matches this with the
    verification result.
27. The RP considers the user to be identified in the session context and
    continues the UX flow.

## Usability Considerations

### Common Considerations

* Relying Party should inform users in advance of what is required for the
  process to be completed successfully and what steps to follow
* For reasons of transparency and to increase trust, Relying Parties should
  provide sufficient information (metadata) for the consent screen. This allows
  users to learn everything relevant e.g. about the relying party itself,
  privacy and data retention
* The user's data values for the requested attributes can be displayed on the
  consent screen
* The user only needs to confirm the (Q)EAA presentation with the Device
  Authenticator

### Same Device Considerations

* It needs to be clarified whether the wallet app needs to be unlocked in this
  flow, as the device authenticator is requested again directly after the
  consent screen. This might create friction and feel redundant to users
* It must be ensured that users are returned to the correct tab in the correct
  browser in order to continue the process or users know how to get there
  manually, if necessary (especially for iOS devices, if the process was not
  started in the default browser)

### Cross Device Considerations

* It needs to be clarified whether the wallet app needs to be unlocked in this
  flow and how this should be implemented.
* User can then continue with the process on the RP page on the second device
  (page must be updated accordingly by the RP)

## Privacy Considerations

* Selective Disclosure is achieved by ensuring that the presentation contains
  only those data items (claims) that are requested by the RP. The selective
  disclosure features of SD-JWT VC are used to this end.
* Unlinkability has to be achieved with batch issuance
* In Presentation Step 5, a malicious app may spoof the wallet app; an attacker
  may
    * **on his own device:** Capture the request and replay it to a victim on
    another device, thus having the victim identify itself in a context of the
    attacker (**Relaying Attack breaking Identification Context**); or
    * **on the victim's device:** Capture the request and spoof the whole
    identification process or parts of it (**Wallet App Spoofing**).
* In Presentation Step 5: An attacker acting as an RP can forward a request from
  a different RP.
    * As long as the request remains unchanged, we're in the **Relaying Attack
    breaking Identification Context**
    * If the attacker changes anything in the request, this will break the
    signature. The attacker could otherwise attempt to
        * insert his own ephemeral key, leading to an SD-JWT VC artifact that could
      be used in a different flow between the attacker and some other RP.
        * modify state or nonce or other data. **Q:** Any useful attacks
      resulting from this?
* In Presentation Step 14: The RP must not consider the user identified at this
  point; it is important to have the browser redirect in the later steps.

## Security Considerations

*TBD*
