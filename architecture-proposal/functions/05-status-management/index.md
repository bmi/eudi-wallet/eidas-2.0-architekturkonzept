# Status Management

Revocation of credentials is a process in which the issuer of PID or (Q)EAA
declares that the credential is no longer valid. Various use cases and scenarios
may require revocation:

- the PID/(Q)EAA Provider wants to revoke its issued credential because the
  contained data is no longer valid
- the Wallet Provider wants to revoke a Wallet Instance because Wallet Security
  Cryptographic Device (WSCD) or the Wallet Instance application is compromised
  or vulnerable
- the user wants to revoke their Wallet Instance because they lost their
  smartphone
- the user wants to revoke their PID
- the PID Provider wants to revoke a PID because the person has died

Regarding the various PID options, only the issuer-signed variations requires
revocation as the Authenticated Channel variations generate fresh credentials
for every transaction.

To enable revocation for credentials within the EUDI Wallet ecosystem, the
following mechanisms for revocation are considered:

- Certificate Revocation Lists
- Status Lists

For the revocation of relying party or issuer certificates, the following
mechanisms are

- Online Certificate Status Protocol (OCSP)
- OCSP stapling

## Mechanisms

### Certificate Revocation Lists

Certificate Revocation Lists (CRL) as defined in
[RFC5280](https://datatracker.ietf.org/doc/rfc5280/) are an established
revocation mechanism for X509 certificate that has been proven in productive
environments for a long time. The Issuer maintains a publicly available list
that contains the serial numbers of revoked certificates. Relying Parties may
discover this list (e.g. as the URL is contained in the certificate) and
validate whether the serial number exists in the list.

As the issuer of a certificate always delivers the complete list, he will not
learn which serial number the Relying Party is interested in, thus guaranteeing
the unobservability. As CRLs are currently only used for X509 certificates, it
remains open to define how this mechanism is used for the PID credential
formats. Additionally, CRLs have seen some scalability limitations in the
Browser TLS context, and it remains open to evaluate if CRL sizes remain
manageable within the eIDAS ecosystem. Usage of CRL Delta updates may be
considered to increase the scalability.

### Status List

[IETF Token Status
List](https://datatracker.ietf.org/doc/draft-ietf-oauth-status-list/) is a new
mechanism with similar properties to CRLs, which defines a status mechanism for
JOSE and COSE secured Referenced Tokens. This status links to a Status List in
JSON and CBOR formats that describe the individual statuses of multiple
Referenced Tokens. The statuses of all Referenced Tokens are conveyed via a bit
array in the Status List. Each Referenced Token is allocated an index during
issuance that represents its position within this bit array. The value of the
bit(s) at this index correspond to the Referenced Token's status. The Status
List is provided by the issuer through a public endpoint or as a signed Status
List Token in JWT or CWT.

As the issuer of a status list always delivers the complete list, he will not
learn which index the Relying Party is interested in, thus guaranteeing the
unobservability. Status List have shown to have better scalability properties
than CRLs under most circumstances, but it remains open to evaluate if this is
sufficient for the eIDAS ecosystem. Status Lists have not been widely adopted
and proven in productive ecosystems yet. The Status List requires additional
communication between the issuance and revocation service during the issuance.

### Online Certificate Status Protocol (OCSP)

In the Online Certificate Status Protocol the issuer provides a service(called
OSCP Responder), which the Relying Party may use to query the revocation status
of a certificate in real time.

As the Relying Party provides the specific certificate serial number to the
issuer, unobservability can not be achieved. As OCSP is currently only used for
X509 certificates, it remains open to define how this mechanism is used for the
PID credential formats. Furthermore, the system has shown to have bad
scalability properties. Therefore, usage of OSCP is *not* recommended.

### OCSP stapling

In the OCSP stapling the TLS certificate owner regularly queries the issuer's
OCSP endpoint and "staples" the result to the response of the TLS handshake,
thus the other party does not need to fetch it themselves, reducing the traffic
and cost for the OCSP Responder.

OCSP stapling was introduced to tackle the scalability issues of OCSP but only
with modest success. OCSP stapling is currently only defined for X509
certificates, but a [proposal applying the concepts to the PID credential
formats is under
development](https://datatracker.ietf.org/doc/draft-demarco-oauth-status-attestations/).
The concept has the privacy potential as the Relying Party does not fetch the
revocation information itself, but it remains open whether sufficient
scalability can be achieved.

## Validation procedure

Before validating the status of a credential, the Relying Party should check if
the credential is not expired yet. If so, the credential is considered invalid
which is defined by either being expired or revoked.

## Privacy Considerations

It is worth to take a look at the privacy consideration of the different methods
like the one from [Token Status
List](https://drafts.oauth.net/draft-ietf-oauth-status-list/draft-ietf-oauth-status-list.html#name-privacy-considerations).

## Recommendations

When using either CRLs or Status Lists, the issuer needs to find the balance
between privacy and required bandwidth. A large CRL or Status List give herd
immunity when feting the list. However, when the list is too large, the Relying
Party or the Wallet eed to download a lot of data frequently.

To reduce the frequency of fetching the list, the issuer is able to define a
Time to Live (TTL) for the list. The Relying Party or Wallet should cache the
list for the defined TTL. The issuer should also provide a mechanism to fetch
the list before the TTL expires.

To calculate the maximum size of the list, the issuer has to calculate the worst
case scenario.

When using a CRL the worst case is the revocation of all credentials. In this
case the CRL includes all the identifiers of the revoked credentials. The size
depends on the algorithm to generate these identifiers (UUID, SHA256, etc.).
**Example**: A UUID has a size of 16 bytes. If the issuer has issued 1 million.
The CRL has a size of 16 MB.

In case of the Status List, the worst case is a state where the compression
algorithm finds no pattern to reduce the size. This is not the case when all
credentials are revoked, because then all values are the same and can be
compressed to a single bit. **Example**: If the issuer has issued 1 million
credentials, the Status List has a size of 125 KB, when using a 1 bit per
credential. In case the status list uses n bits per credential to manage more
than two states, the size is `125 KB * n`.

When the issuer has reached the defined size, it should consider to start with a
parallel list. It is not recommended to reuse the bit of a credential that is
expired, because it is not guaranteed that the Relying Party or Wallet has
fetched the list before. It is the same for the CRL to reduce the risque that an
identifier is reused and creates a false positive case.

To minimize traceability, the issuer should update the revocation list in
batches. For example, if the issuer revokes a batch of 50 credentials, Relying
Parties could deduce that these 50 entries are linked to the same batch issued
to a single wallet, potentially enabling them to track the wallet. To mitigate
this risk, the issuer should include decoy entries in the list. These decoy
entries do not correspond to any actual credentials and serve solely to increase
the apparent size of credential updates, making it unclear to Relying Parties
which entries belong to a specific credential batch.