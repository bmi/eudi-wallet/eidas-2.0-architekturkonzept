<!-- PDF-show
\newpage
-->

# Ecosystem Governance and Operating Model

The revised eIDAS Regulation obliges the member states to provide EUDI wallets.
It specifies three options for the provision (Article 6a revised eIDAS
Regulation 2):

> *European Digital Identity Wallets shall be provided:*
>
> *(a) directly by a Member State;*
>
> *(b) under a mandate from a Member State;*
>
> *(c) independently of a Member State but recognized by that Member State.*

The project on the German implementation of the EUDI Wallet is analyzing which
of these three options could be the most promising and financially sustainable
and which legal framework conditions could be helpful for this success. The
relevant roles in the context of the provision of the EUDI wallet are defined in
Art. 3 revised eIDAS Regulation. In addition to the role of the wallet provider,
the revised eIDAS Regulation also defines other roles that must be recognized
when thinking about a wallet ecosystem:

- Provider of person identification data (PID), Issuer of qualified or
    non-qualified electronic attestation of attributes ((Q)EAA)

- Relying Parties

- QTSPs

- Authentic Sources

The revised eIDAS Regulation has specific regulatory requirements for the
various roles and differentiates between qualified and non-qualified electronic
attributes in terms of governance. This differentiation and the possible designs
of domain-specific ecosystems on this basis are considered in the following.

## EUDI Wallet Ecosystem: Design & Governance

The discussion during the consultation process demonstrated a clear need for a
description of the target Wallet Ecosystem outlining the interaction of the EUDI
Wallet with other entities in the ecosystem. It became evident that most
participants in the consultation process were interested to build applications
on top of the EUDI Wallet infrastructure but did not intend to build and operate
an EUDI Wallet. The purpose of this chapter is to differentiate between those
perspectives and to clearly describe the responsibilities and opportunities for
EUDI Wallet Providers with the intent to gather possible EUDI Wallet providers
for a more detailed discussion on how the ecosystem can be set up and operated.

To differentiate between business and operating models for the EUDI Wallet on
the one hand and its deployment into domains on the other, the EUDI wallet
ecosystem distinguishes between two layers: First the EUDI Wallet layer and
second, the domain layer - illustrated by *Figure 1* within section
[Overview](./01-architecture-proposal.md#overview).

Use cases are implemented by ecosystem participants as required on top of the
well-defined and generic functions of EUDI Wallets - previously described in
section [Wallet Functions](./01-architecture-proposal.md#wallet-functions).
Those use cases are business processes that are relying on the functions of the
EUDIW. Whereas the functions are generic, their content may depend on the
application domain in which they are used. For use cases that take place
exclusively within the eIDAS regulated framework, no additional domain-specific
governance is required. In order to guarantee a common level of trust between
the different actors involved in a domain, domain specific rules and regulations
may be defined for specifying a domain specific compliance regime. Especially,
if business specific data are presented in an attestation that is not regulated
by the eIDAS ecosystem, a specific trust model must be defined and operated for
that business domain. Examples for domains are international travel, banking,
telecommunications, health care, e-government, justice, media etc.

**Domains are formed by groups of similar or industry-specific use cases.** For
example, e-prescriptions and health insurance applications are part of the
\"Health\" domain, account openings and bank transfers are part of the
\"Banking\" Domain.

The **Domain Owner** is responsible for the domain specific **trust
management**. This includes the registration of authentic sources who are
entitled to issue domain specific attestations (EAA). The development of the
regulations applicable to Domains is the responsibility of the domain owners. It
will not be addressed within this project.

**Domain operators are mandated by the respective owners of the domain.** These
can be interest groups, associations or organizations such as health insurance
companies, banks or industry associations. They are responsible for the
governance and the semantics that apply in the Domain and the preconditions that
are relevant for interoperability. They may take the responsibility to publish
the (Q)EAA schema relevant for their domain.

**The different types of (Q)EAAs**, e.g. university certificates, flight
tickets, tax information or health data, form the core of the domains in
conjunction with the application rules applicable to the domain. Domain specific
authentic sources for domain specific attributes are subject to the domain
specific governance.

**(Q)EAA Issuers** provide attestations according to their individual policy.

A policy for **PID Providers** is given by eIDAS Regulation.

The **Relying Party (RP)** can use the domain specific **trust management
mechanisms** to determine whether the domain specific (Q)EAA Issuer is
authorized to issue certain EAAs that are accepted by all stakeholders in the
domain.

It is not necessary to operate a dedicated EUDI Wallet for a Domain as the
definition of the (Q)EAA types by the EUDIW and the related (domain specific)
trust framework should be sufficient to support all uses cases in all Domains.
The architecture of the EUDIW is flexible enough to embed different domain
specific data formats into the application independent Wallet infrastructure.

## EUDI Wallet Provider: Requirements and responsibilities

Based on the ecosystem description outlined above, the EUDIW Providers will have
to fulfill a set of requirements and responsibilities:

**Development** and **operation** of an EUDI wallet, including all necessary
frontend and backend components to provide the wallet service, **compliant**
with the **ARF, the implementing acts** and the national architecture
specification.

**Provision** of **other services** related to the EUDI wallet, e.g. support and
service for ecosystem participants and end user help desk.

**Successful completion** of the yet-to-be-defined **certification process.**

**Provision** of a verifiable and **sustainable funding model** to ensure
long-term development and operation of the EUDIW

## EUDIW Ecosystem Infrastructure

The EUDIW ecosystem must provide the following base services to facilitate
secure interactions for the functions listed within the
[Overview](./01-architecture-proposal.md#overview):

- Identification and authentication of users on LoA High using the German eID
    functionality.
- Identification and authentication of Relying Parties: This enables the EUDIW
    to inform the user about what entity requested their data so that the user
    can make an informed decision whether to allow the transaction.
- Attestation of EUDI Wallets towards Issuers: This enables the Issuers to
    determine whether a party is a genuine EUDI Wallet. Since EUDI Wallets need
    to be certified and licensed, the Issuer gets assurance that the Wallet
    fulfills the relevant regulatory requirements (e.g., key management on
    certain security levels) and the Issuer can trust the Wallet to manage its
    attribute attestations securely and in a privacy preserving manner. Relying
    Parties need the same kind of assurance in any use case, e.g., through a
    "Wallet Instance Attestation" (WIA) during presentation (see Wallet Trust
    Management).
- Trusted List of PID Providers: A "PID Provider" is an entity issuing a PID. A
    Trusted List of PID Providers is the basis for RPs to determine whether they
    can trust a certain PID Provider.

The management of Issuers for EAAs is out of scope of the core infrastructure
since, according to the design principles described above, the lifecycle and
governance process for EAAs is defined in the respective domain and should not
be limited unnecessarily by this ecosystem architecture. Synergies could be
leveraged if the same technical standards as for PID Provider Trust management
can be used.

*Note: The business model stream might evaluate whether that infrastructure
should be left to the market.*

Furthermore, this document mentions the processes of identification and
authentication in examples, explanations, etc. For some examples, authentication
and identification have to be combined, because they depend on the services
built on top of the functions of the EUDIW and also of the state the user is
currently in. For example, if a banking service requests credentials from a user
for the first time, it would be a combined identification and authentication
process. If later on, the same credentials are presented again, it would be a
simple authentication process. As from the wallet perspective however, in any
case, it is always just a presentation of credentials at a service/RP.
